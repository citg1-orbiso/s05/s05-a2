package com.zuitt;

import java.io.IOException;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

@WebServlet("/registration")
public class RegisterServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4957551653977008154L;
	
	public void init() throws ServletException{
		System.out.println("RegisterServlet has been initialized.");
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException {
		// Capture the user input from the booking form
		String firstName = req.getParameter("firstName");
		String lastName = req.getParameter("lastName");
		String phone = req.getParameter("phone");
		String email = req.getParameter("email");
		String appDiscovery = req.getParameter("app_discorvey");
		String dateBirth = req.getParameter("date_of_birth");
		String userType = req.getParameter("user_type");
		String description = req.getParameter("description");
		
		// Store all the data from the form into the session
		HttpSession session = req.getSession();
		
		session.setAttribute("firstName", firstName);
		session.setAttribute("lastName", lastName);
		session.setAttribute("phone", phone);
		session.setAttribute("email", email);
		session.setAttribute("appDiscovery", appDiscovery);
		session.setAttribute("dateBirth", dateBirth);
		session.setAttribute("userType", userType);
		session.setAttribute("description", description);
		
		res.sendRedirect("register.jsp");
		
	}
	
	public void destroy() {
		System.out.println("RegisterServlet has been finalized.");
	}

}

