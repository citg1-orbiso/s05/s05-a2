<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Home</title>
</head>
<body>
			<h1>Welcome <%=session.getAttribute("name") %> !</h1>
			<% 
				String usertype = session.getAttribute("userType").toString();
				if(usertype.equals("Employer"))
					out.println("<p>Welcome employer. You may now start browsing applicant profiles.</p>");	
				else if(usertype.equals("Applicant"))
						out.println("<p>Welcome applicant. You may now start looking for career opportunity.</p>");
			%>
</body>
</html>
