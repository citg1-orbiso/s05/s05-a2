<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>JSP Creating Dynamic Content</title>
		<style>
			div {
				margin-top: 5px;
				margin-bottom: 5px;
			}
			
			#container{
				display: flex;
				flex-direction: column;
				justify-content: center; .
			}
			
			div > input, div > select, div > textarea{
				width: 96%;
			}
			
		</style>
	</head>
	<body>
			
		<div id="container">
		
		<h1>Welcome to Servlet Job Finder!</h1>
		
			<form action="registration" method="post">
					<label for="firstName">First Name</label>
					<input type="text" name="firstName" required>
					<br><br>
					
					<label for="lastName">Last Name</label>
					<input type="text" name="lastName" required>
					<br><br>

					<label for="phone">Phone</label>
					<input type="tel" name="phone" required>
					<br><br>

					<label for="email">Email</label>
					<input type="email" name="email" required>
					<br><br>
								
				<fieldset>
					<legend>How did you discover the app?</legend>
					<input type="radio" id="friends" name="app_discovery" value="friends" required>
					<label for="friends">Friends</label>
					
					<input type="radio" id="social_media" name="app_discorvery" value="socialmedia" required>
					<label for="social_media">Social Media</label>

					<input type="radio" id="others" name="app_discovery" value="others" required>
					<label for="others">Others</label>
				</fieldset>								
				
					<br>
					<label for="date_of_birth">Date of Birth</label>
					<input type="datetime-local" name="date_of_birth" required>
					<br><br>

					<label for="user_type">Are you an employer or applicant?</label>
					<select id="user_chosen" name="user_type">
						<option value="" selected="selected">Select One</option>
						<option value="Employer">Employer</option>
						<option value="Applicant">Applicant</option>
					</select>
					<br><br>
					
					<label for="description">Profile Description</label>
					<textarea name="description" maxlength="500"></textarea>
					<br><br>

				<button>Register</button>
			</form>
		</div>
	</body>
</html>
