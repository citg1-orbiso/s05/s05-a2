<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!-- %@ include file="userType.jsp" %> -->
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Register Confirmation</title>
	</head>
	<body>
		<h1>Register Confirmation</h1>
		<p>First Name: <%= session.getAttribute("firstName") %></p>
		<p>Last Name: <%= session.getAttribute("lastName") %></p>
		<p>Phone: <%= session.getAttribute("phone") %></p>
		<p>Email: <%= session.getAttribute("email") %></p>		
		<p>App Discovery: <%= session.getAttribute("discovery") %></p>
		<p>Date of Birth: <%= session.getAttribute("birthdate") %></p>
		<p>User Type: <%= session.getAttribute("userType") %></p>
		<p>Comments: <%= session.getAttribute("comments") %></p>
		
		<!-- Submit button for booking -->
		<form action="login" method="post">
			<input type="submit">
		</form>
		
		<!-- Button to go back at index.jsp -->
		<form action="index.jsp">
			<input type="submit" value="Back">
		</form>
		
	</body>
</html>
